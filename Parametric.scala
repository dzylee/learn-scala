//This class contains a program that makes use of a function call para() that 
//takes other functions as parameters. The para() function then uses these parameters
//to calculate a set of values and returns the values as a tuple.
object Parametric {

//////// main() method:

    def main(args: Array[String]): Unit = {

        //Setup:

        var xFunc:(Double) => Double = (t: Double) => { 5 * scala.math.cos(t:Double) }        
        var yFunc:(Double) => Double = (t: Double) => { 5 * scala.math.sin(t:Double) }
        var zFunc:(Double) => Double = (t: Double) => { (t:Double)/5 }
        var paraFunc:(Double) => (Double, Double, Double) = para( xFunc, yFunc, zFunc, _:Double )


        def xFuncAlt(t: Double): Double = { 
            return 5 * scala.math.cos(t)
        }
        
        def yFuncAlt(t: Double): Double = { 
            return 5 * scala.math.sin(t)
        }

        def zFuncAlt(t: Double): Double = { 
            return t/5
        }

        var paraFuncAlt:(Double) => (Double, Double, Double) = para( xFuncAlt, yFuncAlt, zFuncAlt, _:Double )


        //Output:        
       
        println("Demonstrating the parametric function\n")
        println("We will use these three parametric equations for this demonstration:")
        println("5*cos(t), 5*sin(t), and t/5, corresponding to the x, y, and z coordinates, respectively." + "\n")        

        println("Each of the three parametric equations are created as function literals and stored in variables:")
            println("var xFunc = (t: Double) => { 5 * scala.math.cos(t:Double) }")
            println("var yFunc = (t: Double) => { 5 * scala.math.sin(t:Double) }")
            println("var zFunc = (t: Double) => { (t:Double)/5 }" + "\n")                
            
            println("Combine all three variables into one function that returns a 3-tuple containing the values for the x, y, and z coordinates, respectively:")
            println("var paraFunc = para( xFunc, yFunc, zFunc, _:Double )" + "\n")
            
            println("Now we can use the variable paraFunc to easily calculate the 3-dimensional coordinates for any value of t.")
            println("For t=18, the result is paraFunc(18):")
            println( paraFunc(18) + "\n")
        
        println("To demonstrate partially applied functions, the three parametric equations could have been defined as functions first, and then these functions could be combined together:")
            println("def xFuncAlt(t: Double): Double = { 5 * scala.math.cos(t) }")
            println("def yFuncAlt(t: Double): Double = { 5 * scala.math.sin(t) }")
            println("def zFuncAlt(t: Double): Double = { t/5 }" + "\n")         
            
            println("var paraFuncAlt = para( xFuncAlt, yFuncAlt, zFuncAlt, _:Double )" + "\n")
            
            println("paraFuncAlt will produce the same result as the previous method. With t=18, the result is paraFuncAlt(18):")
            println( paraFuncAlt(18) + "\n")
    }
    
//////// Parametric equations:

    def para( x: (Double) => Double, 
              y: (Double) => Double, 
              z: (Double) => Double, 
              t: Double 
            ): Tuple3[Double, Double, Double] = {
              
        val xResult:Double = x(t)
        val yResult:Double = y(t)
        val zResult:Double = z(t)
        
        return (xResult, yResult, zResult)
    }
}


