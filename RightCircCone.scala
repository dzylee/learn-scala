//This class contains a program that makes use of two functions that calculate 
//the volume and surface area of a right-circular cone, respectively to demonstrate 
//some of Scala's features in action.
object RightCircCone {

//////// main() method:

    def main(args: Array[String]): Unit = {

        println("Demonstrating the right-circular-cone functions\n")
        
        println("Our cone will have a radius of r=3, and a height of h=10:" + "\n")
        val r:Double = 3
        val h:Double = 10
        
        println("The cone's volume is: coneVol(r, h)")
        println( coneVol(r, h) + "\n" )

        println("The cone's surface area is: coneSurfArea(r, h)")
        println( coneSurfArea(r, h) + "\n" )
    }

//////// Cone volume:

    def coneVol( radius: Double, height: Double ): Double = {       //usage: coneVol(3, 10)
        return scala.math.Pi * radius * radius * height / 3      
    }

//////// Cone surface area:

    def coneSurfArea( radius: Double, height: Double ): Double = {      //usage: coneSurfArea(3, 10)
        return (scala.math.Pi * radius * radius) + (scala.math.Pi * radius * scala.math.sqrt(radius * radius + height * height))
    }
}    


