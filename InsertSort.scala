//This class contains a program that runs two insert-sort algorithms to demonstrate 
//some of Scala's features in action.
object InsertSort {

//////// main() method:

    def main(args: Array[String]): Unit = {
    
        println("Demonstrating insert-sort\n")
        
        val giantList:List[Int] = List(3,54,351,35,65,656,531,-34,265,4656,5,46,75,4,682,35,62256,-53,3,4,-63536,284,78,156,5235,6,65,56,5,43,7,0,-88,56,8,950,94,5,3,523,53,503,570,79,39,7,35,6,4300,21,2,204,304,6463,72,65,8,375,83,36,7,536,643,364,570,120,126,856,-785,65,6856,167)
        println("This is the list being sorted:")
        println( giantList + "\n" )
            
        println("Insert-sort using recursion: insertSort( giantList )")
        println( insertSort( giantList ) + "\n")
        
        println("Insert-sort using tail recursion: insertSortTail( giantList )")
        println( insertSortTail( giantList ) + "\n")    
    }

//////// Insert-sort function using recursion:

    def insertSort(list: List[Int]): List[Int] = {

        def insertValue( n: Int, list: List[Int] ): List[Int] = {
            if( list.isEmpty ) {
                return List(n)
            }
            else if( n > list.head ) {
                return ( list.head :: insertValue(n, list.tail) )
            }
            else {
                return (n :: list)
            }
        }
        
        if( list.isEmpty ) {
            list
        }
        else {
            insertValue( list.head, insertSort(list.tail) )
        }
    }

//////// Insert-sort function using tail recursion:

    def insertSortTail( list: List[Int] ): List[Int] = {
       
        def actualInsertSortTail( accum: List[Int], list: List[Int] ): List[Int] = {
        
            def insertValue( n: Int, list: List[Int] ): List[Int] = {
                if( list.isEmpty ) {
                    return List(n)
                }
                else if( n > list.head ) {
                    return ( list.head :: insertValue(n, list.tail) )
                }
                else {
                    return (n :: list)
                }
            }
            
            if( accum.isEmpty && list.isEmpty ) {
                return List()
            }
            else if( accum.isEmpty ) {
                return actualInsertSortTail( List(list.head), list.tail )
            }
            else if( list.isEmpty ) {
                return accum
            }
            else {
                return actualInsertSortTail( insertValue(list.head, accum), list.tail )
            }
        }
        
        return actualInsertSortTail( List(), list )
    }
}


