//This class contains a program that makes use of a compound-interest calculator
//to demonstrate some of Scala's features in action.
object CompoundInt {

//////// main() method:

    def main(args: Array[String]): Unit = {
    
        println("Demonstrating the compound-interest function\n")
        println("The parameters are principal, annual interest rate, numbers of years in deposit, number of times interest is compounded each year:\n")                
        println("CompountInt.compoundInterest( 1500, 4.3/100, 6, 4 )")                                
        println( CompoundInt.compoundInterest( 1500, 4.3/100, 6, 4 ) + "\n" )
    }
    
//////// The compound-interest function:

    /*
    p = principal amount (the initial amount you borrow or deposit).
    r = annual rate of interest (as a decimal).
    t = number of years the amount is deposited or borrowed for.
    n = number of times the interest is compounded per year  
    returns amount of money accumulated after n years, including interest.
    */
    def compoundInterest( p: Double, r: Double, t: Double, n: Double ): Double = {        //usage: compoundInterest( 1500, 4.3/100, 6, 4 )    ANSWER IS ~1938.84
        return p * scala.math.pow( (1 + r/n), n*t )          //scala.math.pow is a "package object"
    }
}    


