//Some exercises.
object Exercises {

//////// main() method:

    def main(args: Array[String]): Unit = {

        println("The divisors function: Exercises.divisors(1024)")
        println( Exercises.divisors(1024) + "\n")

        println("The primes function: Exercises.primes(100)")
        println( Exercises.primes(100) + "\n")

        println("The join function: Exercises.join(\"aaa\", List(1,2,3))")
        println( Exercises.join("aaa", List(1,2,3)) + "\n")

        println("The pythagorean function: Exercises.pythagorean(20)")
        println( Exercises.pythagorean(30) + "\n")

        println("The hailSeq function: Exercises.hailSeq(11)")
        println( Exercises.hailSeq(11) + "\n")

        println("The merge function: Exercises.merge(List(2,4,6,8), List(1,3,5,7))")
        println( Exercises.merge(List(2,4,6,8), List(1,3,5,7)) + "\n")
        println("The mergesort function: Exercises.mergesort(List(6,2,4,8,9,5,3,1,7,10))")
        println( Exercises.mergesort(List(6,2,4,8,9,5,3,1,7,10)) + "\n")

        println("The fib function: Exercises.fib(10)")
        println( Exercises.fib(10) + "\n")

        println("The recursive fibs function: Exercises.fibs(41)")
        println( Exercises.fibs(41) + "\n")

        println("The fibs function in linear time: Exercises.fibslin(41)")
        println( Exercises.fibslin(41) + "\n")
    }

//////// Divisors:

    def divisors(n: Int): List[Int] = {                                 //usage: Exercises.divisors(1024)
        return for( i <- (2 to (n/2)).toList if n % i == 0 ) yield i
    }

    /*
    //This implementation is incorrect because of List.range():

    def divisors(n: Int): List[Int] = {
        return for( i <- List.range(2, (n/2)) if n % i == 0 ) yield i    //List.range() is equivalent to (i until j)
    }
    */

//////// Primes:

    def primes(n: Int): List[Int] = {                                   //usage: Exercises.primes(100)
        return for( i <- List.range(2, n) if divisors(i).isEmpty ) yield i
    }

//////// Join:

    def join(sep: String, list: List[Any]): String = {          //usage: Exercises.join("aaa", List(1,2,3))
        if( list.isEmpty ) {
            return ""
        }
        else if( list.tail.length == 0 ) {
            return list.head + ""
        }
        else {
            return (list.head + sep + join(sep, list.tail))
        }
    }

//////// Pythagorean:

    def pythagorean(c: Int): List[Tuple3[Int,Int,Int]] = {      //Could also have used "(Int, Int, Int)" in place of "Tuple3[Int,Int,Int]"
        if( c == 0 ) {
            return List((0,0,0))
        }
        else {
            return for( z <- (1 to c).toList; y <- (1 to z).toList; x <- (1 to y).toList if ((x*x + y*y) == z*z) ) yield (x,y,z)
        }
    }

//////// Hailstone sequence:

    def hailSeq(n: Int): List[Int] = {
        if( n == 1 ) {
            return List(1)
        }
        else if( n % 2 == 0 ) {
            return n :: hailSeq( n / 2 )
        }
        else {
            return n :: hailSeq( 3*n + 1 )
        }
    }

//////// Merge:

    def merge( firstList: List[Int], secondList: List[Int] ): List[Int] = {        //usage: Exercises.merge(List(2,4,6,8), List(1,3,5,7))
        if( (firstList.isEmpty) && (secondList.isEmpty) ) {
            return List()
        }
        else if( firstList.isEmpty ) {
            return secondList
        }
        else if( secondList.isEmpty ) {
            return firstList
        }
        else {
            if( firstList.head <= secondList.head ) {
                return firstList.head :: merge( firstList.tail, secondList )
            }
            else {
                return secondList.head :: merge( firstList, secondList.tail )
            }
        }
    }

//////// Merge sort:

    def mergesort( list: List[Int] ): List[Int] = {         //usage: Exercises.mergesort(List(6,2,4,8,9,5,3,1,7,10))
        if( list.length <= 1 ) {
            return list
        }
        else {
            val firstHalf:List[Int] = list.take( list.length / 2 )
            val secondHalf:List[Int] = list.drop( list.length / 2 )
            return merge( mergesort(firstHalf), mergesort(secondHalf) )
        }
    }

    /*
    //Unable to overload the the merge() and mergesort() functions because of Scala's type erasure,
    //which prevents the compiler from distinguishing between List objects of different types.
    //Leaving this code here just to show how the overload would be done if the type erasure problem
    //did not exist.

    def merge( firstList: List[Char], secondList: List[Char] ): List[Char] = {        //usage: Exercises.merge("".toList, "".toList)
        if( (firstList.isEmpty) && (secondList.isEmpty) ) {
            return List()
        }
        else if( firstList.isEmpty ) {
            return secondList
        }
        else if( secondList.isEmpty ) {
            return firstList
        }
        else {
            if( firstList.head <= secondList.head ) {
                return firstList.head :: merge( firstList.tail, secondList )
            }
            else {
                return secondList.head :: merge( firstList, secondList.tail )
            }
        }
    }

    def mergesort( list: List[Char] ): List[Char] = {         //usage: Exercises.mergesort("The quick brown fox jumped over the lazy dog.".toList)
        if( list.length <= 1 ) {
            return list
        }
        else {
            val firstHalf:List[Char] = list.take( list.length / 2 )
            val secondHalf:List[Char] = list.drop( list.length / 2 )
            return merge( mergesort(firstHalf), mergesort(secondHalf) )
        }
    }
    */

//////// Fibonacci:

    def fib( n: Int ): Int = {
        if( n == 0 ) {
            return 0
        }
        else if ( n == 1 ) {
            return 1
        }
        else {
            return fib(n-1) + fib(n-2)
        }
    }

    def fibs( n: Int ): List[Int] = {           //usage: Exercises.fibs(42)
        var result:List[Int] = List()

        for( counter <- (0 to n) ) {
            result = result ::: List( fib(counter) )
        }
        return result
    }

    def fibslin( n: Int ): List[Int] = {        //usage: Exercises.fibslin(42)
        var result:List[Int] = List()

        for( counter <- (0 to n) ) {
            if( counter == 0 ) {
                result = result ::: List(0)
            }
            else if( counter == 1 ) {
                result = result ::: List(1)
            }
            else {
                result = result ::: List( (result.init.last + result.last) )
            }
        }
        return result
    }
}



