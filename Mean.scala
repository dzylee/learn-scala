//This class contains a program that makes use of two functions that calculate 
//two different kinds of mean, to demonstrate some of Scala's features in action: 
//expected value, and arithmetic mean.
object Mean {

//////// main() method:

    def main(args: Array[String]): Unit = {

        println("Demonstrating mean functions\n")
        
        println("We will calculate the expected value for the following (probability, value) pairs:")
            println("(0.12,34),(0.02,14),(0.3,2),(0.44,9),(0.07,93),(0.05,11)\n")
            println("The expected value is: ev( (0.12,34),(0.02,14),(0.3,2),(0.44,9),(0.07,93),(0.05,11) )")
            println( ev( (0.12,34),(0.02,14),(0.3,2),(0.44,9),(0.07,93),(0.05,11) ) + "\n" )
        
        println("We will calculate the expected value for these two lists (one list for probabilities, and the other list for values):")
            println("List(0.12, 0.02, 0.3, 0.44, 0.07, 0.05) and List(34, 14, 2, 9, 93, 11)\n")        
            println("The expected value is: ev(List(0.12, 0.02, 0.3, 0.44, 0.07, 0.05), List(34, 14, 2, 9, 93, 11):List[Double])")
            println( ev(List(0.12, 0.02, 0.3, 0.44, 0.07, 0.05), List(34, 14, 2, 9, 93, 11):List[Double]) + "\n" )

        println("We will calculate the arithmetic mean for the fololowing values:")
            println("34,14,2,9,93,11\n")
            println("The arithmetic mean is: am(34,14,2,9,93,11)")
            println( am(34,14,2,9,93,11) + "\n" )
    }

//////// Expected value

    //The first element in each pair is the probability (or weight), and the second element in each pair is the value.
    def ev(pairs: Tuple2[Double, Double]*): Double = {
        var answer:Double = 0
        var probSum:Double = 0
        
        for( i <- pairs ) {
            answer = answer + (i._1 * i._2)
            probSum = probSum + i._1
        }
        
        if( probSum != 1 ) {
            println("Probabilities do not sum up to 1. Returning 0.")
            return 0
        }
        else {
            return answer
        }
    }

    //The first list consists of the probabilities. The second list consists of the values.
    def ev(probs: List[Double], values: List[Double]): Double = {
                
        if( probs.length != values.length ) {
            println("The number of probabilities and the number of values are not equal. Returning 0.")
            return 0
        }
        else {
            val array_of_pairs:Array[Tuple2[Double, Double]] = probs.zip(values).toArray
            var answer:Double = 0
            var probSum:Double = 0            

            for( i <- 0 until array_of_pairs.length ) {
                answer = answer + ( array_of_pairs(i)._1 * array_of_pairs(i)._2 )
                probSum = probSum + array_of_pairs(i)._1
            }
            
            if( probSum != 1 ) {
                println("Probabilities do not sum up to 1. Returning 0.")
                return 0
            }
            else {
                return answer
            } 
        }
    }

//////// Arithmetic mean:

    def am(numbers: Double*): Double = {
        var sum:Double = 0
        
        for( i <- numbers ) {
            sum = sum + i
        }
        return sum / numbers.length
    }
}    
